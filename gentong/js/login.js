
$(document).ready(function(){
     //jika form sudah disubmit
     $("#loginForm").submit(function(){
          //remove all the class add the messagebox classes and start fading
          $.blockUI({ message: ' Validasi...' });
          setTimeout(function() {
               $.unblockUI({
                    onUnblock: function(){
                         $.ajax({
                              type: 'POST',
                              url: "login.svr.pln",
                              dataType:"json",
                              data: $("#loginForm").serialize(),
                              cache:false,
                              //jika complete maka
                              complete:
                              function(data,status){
                                   return false;
                              },
                              success:
                                   function(msg,status){
					//alert(msg);
                                        loginData = msg;
					//alert(msg);
                                        //alert($("#loginForm").serialize());
                                        //jika menghasilkan result
                                        if(loginData.result == 'success'){
                                             //jika login berhasil
                                             if(loginData['data'].status == 'success'){
                                                  $.blockUI({ message: ' Login successfull' });
                                                  setTimeout(
                                                       function() {
                                                            $.unblockUI();
                                                            document.location='index.pln';
                                                       }, 1000
                                                  );
                                             //jika login gagal
                                             }else{
                                                  sDesc = "";
                                                  $.each(loginData['data'].desc, function(i) {
                                                       sDesc += loginData['data']['desc'][i] + "<BR />";
                                                  });
                                                  jError(sDesc,"Error");
                                             }
                                        }else{
                                             jError(loginData.desc, 'Konfirmasi');
                                        }
                                   },
                              //untuk sementara tidak digunakan
                              error:
                                   function(msg,textStatus, errorThrown){
                                        jError('Terjadi error saat proses kirim data', 'Konfirmasi');
                                   }
                         });//end of ajax
                    }
               });
          }, 2000); 
          return false; //not to post the  form physically
     });
     //now call the ajax also focus move from
     $("#password").blur(function(){
          $("#login_form").trigger('submit');
     });
});
